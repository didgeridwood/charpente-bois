import unittest
import math
import P1


class TesteAretier(unittest.TestCase):
    def testrdd (self):
        listedonnee = P1.recupDonnee()
        reculVersants = listedonnee[0]
        hauteur = listedonnee[1]
        longueurAretierSol = math.sqrt((reculVersants * reculVersants) + (reculVersants * reculVersants))  # pythagore
        longAretier = math.sqrt((longueurAretierSol * longueurAretierSol) + (hauteur * hauteur))  # pythagore
        AangleAretierPied = math.asin(hauteur / longAretier)
        angleAretierTete = math.atan(longueurAretierSol / hauteur)
        retombAretier = hauteur * math.sin(angleAretierTete)
        hypp = math.sqrt((longueurAretierSol * longueurAretierSol) + (retombAretier * retombAretier))  # pythagore
        delardFace = math.asin(longueurAretierSol / hypp)
        delardementTotal = delardFace * 2

        # DICTIONNAIRE TESTE
        result = {}
        result["delardFace B0 "] = (round(math.degrees(delardFace), 2))
        result["delardementTotal B2 "] = (round(math.degrees(delardementTotal), 2))


        self.assertEqual(P1.rdd(reculVersants,hauteur),result )


if __name__ == '__main__':
    unittest.main()
