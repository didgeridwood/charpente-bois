import math

#P1 Rectiligne du dièdre (calcule du delardement de l'arretier)
#Etude 1

#donnee user
reculVersants, hauteur = 88, 55


def recupDonnee ():
    listeDonnee=[reculVersants, hauteur]
    return listeDonnee

def rdd (reculVersants, hauteur):
#Rectiligne du dièdre (calcule du delardement de l'arretier)

#CALCULES DES ANGLES
    longueurAretierSol = reculVersants * math.sqrt(2)
    AangleAretierPied = math.atan(hauteur/longueurAretierSol)
    retombAretier =longueurAretierSol* math.sin(AangleAretierPied)
    delardFace = math.atan(longueurAretierSol/retombAretier)
    delardementTotal = delardFace*2

#DICTIONAIRE angles en degrees
    result = {}
    result["delardFace B0 "]=(round(math.degrees(delardFace),2))
    result["delardementTotal B2 "]=(round(math.degrees(delardementTotal),2))
    print(result)

    return result

print ("tout les angles sont en degrees")
rdd(reculVersants, hauteur)
print(recupDonnee())
