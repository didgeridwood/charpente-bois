import math
import unittest
import Aretier

class AretierTeste(unittest.TestCase):

    def testeLongueurAretiersol(self):
        coteA=Aretier.dicoDonnee.get("reculVersantsA")
        coteB=Aretier.dicoDonnee.get("reculVersantsB")
        result = math.sqrt((coteA * coteA) + (coteB * coteB))
        dicoValue=Aretier.dicoDonnee.get("longueurAretierSol")
        self.assertEqual(result,dicoValue)

    def testelongAretierAxe(self):
        longueurAretierSol = Aretier.dicoDonnee.get("longueurAretierSol")
        hauteur = Aretier.dicoDonnee.get("hauteur")
        result = math.sqrt((longueurAretierSol * longueurAretierSol) + (hauteur * hauteur))
        dicoValue = Aretier.dicoDonnee.get("longAretierAxe")
        self.assertEqual(result,dicoValue)

    def testeAngleAplonAretier(self):
        longueurSol = Aretier.dicoDonnee.get("longueurAretierSol")
        hauteur = Aretier.dicoDonnee.get("hauteur")
        result = math.atan(longueurSol / hauteur)
        dicoValue = Aretier.dicoDonnee.get("angleAplonAretier")
        self.assertEqual(result,dicoValue)

    def testeAngleNiveauAretier(self):
        angleDroit= 90
        angleNiveauAretier= Aretier.dicoDonnee.get("angleNiveauAretier")
        angleAplonAretier = Aretier.dicoDonnee.get("angleAplonAretier")
        self.assertEqual(angleDroit,math.degrees(angleNiveauAretier+angleAplonAretier))

    def testeAngleCoupemachineA(self):
        reculVersantsA = Aretier.dicoDonnee.get("reculVersantsA")
        reculVersantsB = Aretier.dicoDonnee.get("reculVersantsB")
        result = math.atan(reculVersantsB /reculVersantsA)
        dicoValue = Aretier.dicoDonnee.get("angleCoupemachineA")
        self.assertEqual(round(result,8),round(dicoValue,8))

    def testeAngleCoupemachineB(self):
        reculVersantsA = Aretier.dicoDonnee.get("reculVersantsA")
        reculVersantsB = Aretier.dicoDonnee.get("reculVersantsB")
        result = math.atan(reculVersantsA /reculVersantsB)
        dicoValue = Aretier.dicoDonnee.get("angleCoupemachineB")
        self.assertEqual(round(result,8),round(dicoValue,8))

    def testeAngleNiveauAretier(self):
        angleDroit= 90
        angleCoupemachineB = Aretier.dicoDonnee.get("angleCoupemachineB")
        angleCoupemachineA = Aretier.dicoDonnee.get("angleCoupemachineA")
        self.assertEqual(angleDroit,round(math.degrees(angleCoupemachineA+angleCoupemachineB),8))

    def testeRetombVueParBoutAretier(self):
        hauteur = Aretier.dicoDonnee.get("hauteur")
        angleAplonAretier = Aretier.dicoDonnee.get("angleAplonAretier")
        result = hauteur * math.sin(angleAplonAretier)
        dicoValue = Aretier.dicoDonnee.get("retombVueParBoutAretier")
        self.assertEqual(round(result,8),round(dicoValue,8))

    def testeLongSoterelleVersB(self):
        angleCoupemachineB = Aretier.dicoDonnee.get("angleCoupemachineB")
        longueurAretierSol = Aretier.dicoDonnee.get("longueurAretierSol")
        result = longueurAretierSol/math.tan(angleCoupemachineB)
        dicoValue = Aretier.dicoDonnee.get("longSoterelleVersB")
        self.assertEqual(round(result,8),round(dicoValue,8))

    def testeLongSoterelleVersA(self):
        angleCoupemachineA = Aretier.dicoDonnee.get("angleCoupemachineA")
        longueurAretierSol = Aretier.dicoDonnee.get("longueurAretierSol")
        result = longueurAretierSol/math.tan(angleCoupemachineA)
        dicoValue = Aretier.dicoDonnee.get("longSoterelleVersA")
        self.assertEqual(round(result,8),round(dicoValue,8))

    def testeAngleDelardementVersA(self):
        angleDroit=90
        retombVueParBoutAretier = Aretier.dicoDonnee.get("retombVueParBoutAretier")
        longSoterelleVersA = Aretier.dicoDonnee.get("longSoterelleVersA")
        result = math.atan(retombVueParBoutAretier/longSoterelleVersA)
        self.assertEqual(angleDroit,math.degrees(result+Aretier.angleDelardementVersA()))

    def testeAngleDelardementVersA(self):
        angleDroit=90
        retombVueParBoutAretier = Aretier.dicoDonnee.get("retombVueParBoutAretier")
        longSoterelleVersB = Aretier.dicoDonnee.get("longSoterelleVersB")
        result = math.atan(retombVueParBoutAretier/longSoterelleVersB)
        self.assertEqual(angleDroit,math.degrees(result+Aretier.angleDelardementVersB()))

    def testeAnglePenteVerssantB(self):
        angleDroit = 90
        hauteur = Aretier.dicoDonnee.get("hauteur")
        reculVersantsB = Aretier.dicoDonnee.get("reculVersantsB")
        result = math.atan(reculVersantsB/hauteur)
        dicoValue = Aretier.dicoDonnee.get("anglePenteVerssantB")
        self.assertEqual(angleDroit,math.degrees(dicoValue+result))

    def testeAnglePenteVerssantA(self):
        angleDroit = 90
        hauteur = Aretier.dicoDonnee.get("hauteur")
        reculVersantsA = Aretier.dicoDonnee.get("reculVersantsA")
        result = math.atan(reculVersantsA/hauteur)
        dicoValue = Aretier.dicoDonnee.get("anglePenteVerssantA")
        self.assertEqual(angleDroit,math.degrees(dicoValue+result))

if __name__ == '__main__':
    unittest.main()