import math
#CALCUL DE L'ARETIER avec les murs en angle droit

#donnee user
reculVersantsA = 500
reculVersantsB = 350
hauteur = 500
chantAretier = 30
epaiseurAretier = 10
sectionPoiconCarre = 20

dicoDonnee={"reculVersantsA" : reculVersantsA ,
            "reculVersantsB" : reculVersantsB,
            "hauteur" : hauteur,
            "chantAretier" : chantAretier,
            "epaiseurAretier" : epaiseurAretier,
            "sectionPoiconCarre" : sectionPoiconCarre
            }

def recupDonnee ():
    return print(dicoDonnee)

def longueurAretierSol():
    reculVersantsA = dicoDonnee.get("reculVersantsA")
    reculVersantsB = dicoDonnee.get("reculVersantsB")
    result = math.sqrt((reculVersantsA * reculVersantsA) + (reculVersantsB * reculVersantsB))#pythagore
    dicoDonnee["longueurAretierSol"] = result
    print ("longueurAretierSol",round(result,2)," cm")
    return result

def longAretierAxe():
    hauteur =dicoDonnee.get("hauteur")
    longueurAretierSol = dicoDonnee.get("longueurAretierSol")
    result = math.sqrt((longueurAretierSol * longueurAretierSol) + (hauteur * hauteur))#pythagore
    dicoDonnee["longAretierAxe"] = result
    print("longAretierAxe",round(result,2)," cm")
    return result

def angleAplonAretier():
    hauteur = dicoDonnee.get("hauteur")
    longueurSol=dicoDonnee.get("longueurAretierSol")
    result = math.atan( longueurSol/hauteur)
    dicoDonnee["angleAplonAretier"] = result
    print("angleAplonAretier", round(math.degrees(result),2), "°degrees")
    return result

def angleNiveauAretier():
    hauteur = dicoDonnee.get("hauteur")
    longueurSol = dicoDonnee.get("longueurAretierSol")
    result = math.atan( hauteur/longueurSol)
    dicoDonnee["angleNiveauAretier"] = result
    print("angleNiveauAretier", round(math.degrees(result),2), "°degrees")
    return result

def angleCoupemachineA():
    reculVersantsA = dicoDonnee.get("reculVersantsA")
    longueurAretierSol = dicoDonnee.get("longueurAretierSol")
    result = math.acos(reculVersantsA/longueurAretierSol)
    dicoDonnee["angleCoupemachineA"] = result
    print ("angleCoupemachineA", round(math.degrees(result),2), "°degrees")
    return result

def angleCoupemachineB():
    reculVersantsB = dicoDonnee.get("reculVersantsB")
    longueurAretierSol = dicoDonnee.get("longueurAretierSol")
    result = math.acos(reculVersantsB / longueurAretierSol)
    dicoDonnee["angleCoupemachineB"] = result
    print("angleCoupemachineB", round(math.degrees(result),2), "°degrees")
    return result

def retombVueParBoutAretier():#angleNiveauAretier + longueurAretierSol
    angleNiveauAretier = dicoDonnee.get("angleNiveauAretier")
    longueurAretierSol = dicoDonnee.get("longueurAretierSol")
    result = longueurAretierSol * math.sin(angleNiveauAretier)
    dicoDonnee["retombVueParBoutAretier"] = result
    print("retombVueParBoutAretier",round(result,2), "cm")
    return result

def longSoterelleVersA():
    longueurAretierSol = dicoDonnee.get("longueurAretierSol")
    angleCoupemachineB = dicoDonnee.get("angleCoupemachineB")
    result = longueurAretierSol * math.tan(angleCoupemachineB)
    dicoDonnee["longSoterelleVersA"] = result
    print("longSoterelleVersA",round(result,2), "cm")
    return result

def longSoterelleVersB():
    longueurAretierSol = dicoDonnee.get("longueurAretierSol")
    angleCoupemachineA = dicoDonnee.get("angleCoupemachineA")
    result = longueurAretierSol * math.tan(angleCoupemachineA)
    dicoDonnee["longSoterelleVersB"] = result
    print("longSoterelleVersB",round(result,2), "cm")
    return result

def angleDelardementVersA():
    retombVueParBoutAretier = dicoDonnee.get("retombVueParBoutAretier")
    longSoterelleVersA = dicoDonnee.get("longSoterelleVersA")
    result = math.atan(longSoterelleVersA/retombVueParBoutAretier)
    dicoDonnee["angleDelardementVersA"] = result
    print("angleDelardementVersA", round(math.degrees(result),2), "°degrees")
    return result

def angleDelardementVersB():
    retombVueParBoutAretier = dicoDonnee.get("retombVueParBoutAretier")
    longSoterelleVersB = dicoDonnee.get("longSoterelleVersB")
    result = math.atan(longSoterelleVersB/retombVueParBoutAretier)
    dicoDonnee["angleDelardementVersB"] = result
    print("angleDelardementVersB", round(math.degrees(result),2), "°degrees")
    return result

def angleDelardementTotal():# sans teste
    result = angleDelardementVersA()+angleDelardementVersB()
    dicoDonnee["angleDelardementTotal"] = result
    print("angleDelardementTotal", round(math.degrees(result),2), "°degrees")
    return result

def anglePenteVerssantA():
    hauteur = dicoDonnee.get("hauteur")
    reculVersantsA = dicoDonnee.get("reculVersantsA")
    result = math.atan(hauteur/reculVersantsA)
    dicoDonnee["anglePenteVerssantA"] = result
    print("anglePenteVerssantA", round(math.degrees(result),2), "°degrees")
    return result

def anglePenteVerssantB():
    hauteur = dicoDonnee.get("hauteur")
    reculVersantsB = dicoDonnee.get("reculVersantsB")
    result = math.atan(hauteur/reculVersantsB)
    dicoDonnee["anglePenteVerssantB"] = result
    print("anglePenteVerssantB", round(math.degrees(result),2), "°degrees")
    return result

def pentePour100VerssantA():# sans teste
    reculVersantsA = dicoDonnee.get("reculVersantsA")
    hauteur = dicoDonnee.get("hauteur")
    result = 100 * hauteur/reculVersantsA
    print("pente du verssant A",result,"%")
    return result

def pentePour100VerssantB():# sans teste
    reculVersantsB = dicoDonnee.get("reculVersantsB")
    hauteur = dicoDonnee.get("hauteur")
    result = 100 * hauteur / reculVersantsB
    print("pente du verssant B", result, "%")
    return result


#main
anglePenteVerssantA()
anglePenteVerssantB()
pentePour100VerssantA()
pentePour100VerssantB()
longueurAretierSol()
longAretierAxe()
angleAplonAretier()
angleNiveauAretier()
angleCoupemachineA()
angleCoupemachineB()
retombVueParBoutAretier()
longSoterelleVersB()
longSoterelleVersA()
angleDelardementVersA()
angleDelardementVersB()
angleDelardementTotal()
